import React from 'react';
// import { Link } from "gatsby"

import Header from '../components/header';
import SEO from '../components/seo';
import Footer from '../components/footer';

const SecondPage = () => (
    <div className='mainbody'>
        <SEO title='About' />
        <Header />
        <div className='site-content'>
            <h1>About Us</h1>
            <h3>Here are some History lessons</h3>
            <ul>
                <li><b>FOSS CELL IN NSS</b></li>
                    <p>The FOSS Cell (FOSSNSS) of NSS College of Engineering Palakkad is an association of students and faculties who are vehement about free software 
                        and technology and desire to traverse and proclaim open-source software and its ideology. Origins of this FOSS Cell records back to the year 2009 
                        when this thought was in its mere inception.<br></br><br></br>
                        This initiative aims to strengthen the inadequate areas as compared to global students of computer sciences. One of them is `Competitive Coding`. 
                        `Competitive Coding`, very candidly is a sport and incorporates all benefits of it, namely retaining the brain alert and active. Our vision enlists 
                        stimulating the young minds to think of new ideas and drive them to its fulfilment. We wish to conceive an ambience which encourages students to 
                        take up challenges in the subject, and more importantly have a culture of achieving them, which at the ground level comprises having more than a 
                        prototype; a product along with documentation. Fundamental to all aforesaid, it is about formulating a work culture where we steer the students 
                        towards methodically ascertaining a resolution to their problems and coming up with conclusions rather than just yielding good ideas.


                </p>
                <br></br>
                <li><b>MISSION</b></li>
                    <br></br>
                    <ul>
                        <li>
                        Foster research and advancement in the domain of Free and Open Source Software and the knowledge development model it establishes foremost.
                        </li>
                        <li>
                        To encourage and equip Open Source opportunities
                        </li>
                        <li>
                        To empower all members to actively exercise and partake in open source projects in a beneficial setting.
                        </li>
                    </ul>
                    <br></br>

                <li><b>VISIONS</b></li>
                <br></br>
                <ul>
                    <li>
                    Directing the entire campus to the realm of Freedom, Community and Free Software.
                    </li>
                    <li>
                    To become a preeminent research organisation in Free and Open Source model of knowledge development.
                    </li>
                    <li>
                    Empowering towards sustainable advancement of society and to stimulate economic progress in the region.
                    </li>
                </ul>
                <br></br>
                <br></br>
                <li><b>Our story</b></li>
                <p>In the most pioneering days, Software companies began to restrain access to the source code of their 
                    software, which was an obvious infringement of the human right. It was Richard Stallman an engineer 
                    from MIT who took action, hence the GNU Project was born and he founded the Free Software Foundation. 
                    And after that Stallman decided to contribute majorly for this ambition and made sure that it will 
                    stay free. The GNU project bloomed, and many followed Stallman’s idea and participated and to develop 
                    free software. In 1991, Linus Torvalds, a student from the University of Helsinki in Finland released 
                    the source code for a very simple kernel. Stallman and his folks embraced the Linux as the kernel of 
                    their GNU OS, which is now be known as GNU/Linux and thus FOSS has become universally recommended and 
                    acceptable.</p>
                <p>The philosophy of Stallman has followers in our college as well, as an assemblage of students gathered 
                    to form this FOSS cell in order to partake the ideology of FOSS and help others to acquire about it. 
                    Every ship requires strong sails and an experienced captain to direct and sustain the journey all at 
                    the same time. Faculty advisors: Mr Syam Sankar (CSE Dept.) and Mr Vinod (ECE Dept.) supported the 
                    idea of FOSS helped and guided us to become what we are now.</p>
                <br></br>
                <li><b>Why Should You Join FOSS?</b></li>
                <br></br>
                <ul>
                    <li>
                    Freedom matters! (freedom to create, share, invent, collaborate, learn and modify).
                    </li>
                    <li>
                    Numerous choices, not only what to practice but also how to practice it. Everybody can try and perform it.
                    </li>
                    <li>
                    Loads of community support, one can even acquire an opportunity to talk to the people who developed the software.
                    </li>
                    <li>
                    It promotes cross-platform, fosters authentic innovation and creativity.
                    </li>
                    <li>
                    Keen competition nourishing to keep things move progressively.
                    </li>
                    <li>
                    The most efficacious means to build software as anybody can join in.
                    </li>
                    <li>
                    Has passionate communities full of passionate people.
                    </li>
                    <li>
                    It doesn't exclude anybody and its door is always open.
                    </li>
                    <li>
                    It accommodates everybody, from beginner to expert.
                    </li>
                    <li>
                    It promotes knowledge sharing and allows everybody to learn from each other.
                    </li>
                    <li>
                    It encourages easy customization, as we don't believe one product suits, everybody.
                    </li>
                    <li>
                    It doesn't sacrifice quality as there is no deadline – developers keep working until their product is perfect.
                    </li>
                    <li>Reduce the gap between software "producers" and "consumers".</li>
                    <li>
                    Open standards benefit everyone.
                    </li>
                    <li>
                    Knowledge is meant to be shared.
                    </li>
                    <li>
                    Weave your own code.
                    </li>
                    <li>
                    The corporate world uses it. Big time.
                    </li>
                    <li>
                    It's there everywhere in, the internet infrastructure space.
                    </li>
                    <li>It doesn't encourage the culture of dependency and helplessness.</li>
                    <li>The sharing culture promotes learning.</li>
                    <li>
                    It creates a meritocracy, where racism, class, geography have no place.
                    </li>
                    <li>
                    It promotes amazing diversity in global computing, like no corporate can.
                    </li>
                </ul>
            </ul>
            <br></br><br></br><br></br>
        </div>
        <Footer />
    </div>
);

export default SecondPage;
